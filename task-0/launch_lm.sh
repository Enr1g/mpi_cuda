#!/bin/bash

base="lm"
scratch="$HOME/_scratch"

cpus="128 32 16 8"
grids="1000 2000"

for cpu in $cpus
do
    for grid in $grids
    do
        flag="block"
        while [[ "$flag" == "block" ]]
        do
            job_num=$(./queue_lm.sh | grep test | wc -l)
            if [[ $job_num -lt 3 ]]
            then
                flag="go!"
            fi
            echo "flag=$flag"
            sleep 1
        done
        dir="${base}/${cpu}_${grid}"
        mkdir -p "${scratch}/$dir"
        ./run_lm.sh $cpu $grid $dir
    done
done
