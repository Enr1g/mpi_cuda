#pragma once

#include "GlobalParameters.h"
#include "GridFunction.h"
#include "utils.h"

class Solver {

    // Приближённое решение
    GridFunction _p;

    // Предвычисленное значение F(x, y) на сетке
    GridFunction _F;

    // Предвычисленное значение точного решения
    GridFunction _phi;

    // Невязка
    GridFunction _r;

    /*
     * Один из итерационных параметров метода
     * сопряжённых градиентов
     */
    GridFunction _g;

    // Итерационный параметр
    double _tao;

    // Коэффициент метода сопряжённых градиентов
    double _alpha;

    // Разница между итерациями по максимум норме
    double diff;

    // Количество итераций
    int iter_counter;
public:
    void iteration0();
    void iteration();

    /*
     * Обменяться с другими процессами
     * граничными точками по MPI
     */
    void doExchange(GridFunction &gf);

    /*
     * Найти решение уравнения Пуассона
     */
    int solve();

    /*
     * Записать решения в файл
     */
    void dumpSolution() const;

    /*
     * Печать итерационных параметров
     */
    void print(std::ostream &s) const;
};
