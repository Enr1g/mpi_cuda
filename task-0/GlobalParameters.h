#pragma once

#include <cmath>
#include <iostream>

// Глобальные параметры программы
class GlobalParameters {
public:
    static unsigned int N;
    static double h;
    static double rh;
    
    static int mpi_size;
    static int mpi_rank;

    static unsigned int cpus_h;
    static unsigned int cpus_v;
    static unsigned int cpus_i;
    static unsigned int cpus_j;
    static unsigned int top_neighbour;
    static unsigned int right_neighbour;
    static unsigned int bottom_neighbour;
    static unsigned int left_neighbour;

    static double top;
    static double right;
    static double bottom;
    static double left;

    static double epsilon;

    static char *dump_dir;

    static void Init();

    static void print(std::ostream &s);
};

typedef GlobalParameters gp;
