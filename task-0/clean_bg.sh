#!/bin/bash

set -x

cpus="512 256 128 1"
grids="1000 2000"

base="vanilla"

for cpu in $cpus
do
    for grid in $grids
    do
        dir="${base}/${cpu}_${grid}"
        mkdir -p $dir
        rm $dir/*
    done
done
