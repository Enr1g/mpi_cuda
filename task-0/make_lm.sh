#!/bin/sh

module add impi

set -x

base=$(dirname $0)
scratch="$HOME/_scratch"

cd $base

mpicxx -O3 main.cpp GlobalParameters.cpp GridFunction.cpp Solver.cpp -o poisson_lm
cp poisson_lm "${scratch}/"

set +x

module rm impi
