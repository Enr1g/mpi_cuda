#include "Solver.h"


void Solver::iteration0() {
    iter_counter = 0;

    // Предвычисляем ряд параметров
    _F.genFill<F>();
    _phi.genFill<Phi>();

    // Формируем начальное приближение
    _p.genFill<PhiBorder>();

    /*
     * Считаем невязку, на границе остаются
     * нули, так как _r был инициализирован
     * конструктором по умолчанию
     */
    #pragma omp parallel for
    for (int i = GridFunction::_iCalcBegin; i < GridFunction::_iCalcEnd; ++i) {
        for (int j = GridFunction::_jCalcBegin; j < GridFunction::_jCalcEnd; ++j) {
            _r(i, j) = _p.diffLaplasian(i, j) - _F(i, j);
            _g(i, j) = _r(i, j);
        }
    }
}

void Solver::iteration() {
    double dots[2], res[2];

    iter_counter++;
    diff = 0;

    doExchange(_g);
    dots[0] = _r.dot(_g);
    dots[1] = _g.dot_dlp(_g);

    MPI_Allreduce(&dots, &res, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    _tao = res[0] / res[1];

    /*
     * Проводим итерацию метода.
     */
    #pragma omp parallel
    {
        double local_diff = 0;

        #pragma omp for nowait
        for (int i = GridFunction::_iCalcBegin; i < GridFunction::_iCalcEnd; ++i) {
            for (int j = GridFunction::_jCalcBegin; j < GridFunction::_jCalcEnd; ++j) {
                double delta = _tao * _g(i, j);
                local_diff = std::max(local_diff, fabs(delta));
                _p(i, j) = _p(i, j) - delta;
            }
        }

        #pragma omp critical
        {
            diff = std::max(diff, local_diff);
        }
    }

    /*
     * Считаем невязку
     */
    doExchange(_p);
    #pragma omp parallel for
    for (int i = GridFunction::_iCalcBegin; i < GridFunction::_iCalcEnd; ++i) {
        for (int j = GridFunction::_jCalcBegin; j < GridFunction::_jCalcEnd; ++j) {
            _r(i, j) = _p.diffLaplasian(i, j) - _F(i, j);
        }
    }

    doExchange(_r);
    dots[0] = _r.dot_dlp(_g);
    // dots[1] = _g.dot_dlp(_g);

    MPI_Allreduce(&dots, &res, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    _alpha = res[0] / res[1];

    #pragma omp parallel for
    for (int i = GridFunction::_iCalcBegin; i < GridFunction::_iCalcEnd; ++i) {
        for (int j = GridFunction::_jCalcBegin; j < GridFunction::_jCalcEnd; ++j) {
            _g(i, j) = _r(i, j) - _alpha * _g(i, j);
        }
    }
}

void Solver::doExchange(GridFunction &gf) {
    double right_col[2][_p.rows];
    double left_col[2][_p.rows];
    MPI_Request recv_req[4], send_request[4];
    unsigned int size = 0;

    // Отправляем свои данные
    // top
    if (gp::cpus_i > 0) {
        check_mpi(
            MPI_Isend(gf.topCalc, gf.columns, MPI_DOUBLE, gp::top_neighbour, 0, MPI_COMM_WORLD, &send_request[size]),
            "MPI_Isend()::top"
        );

        check_mpi(
            MPI_Irecv(gf.topStor, gf.columns, MPI_DOUBLE, gp::top_neighbour, MPI_ANY_TAG, MPI_COMM_WORLD, &recv_req[size++]),
            "MPI_Recv()::top"
        );
    }

    // right
    if (gp::cpus_j < gp::cpus_h - 1) {
        double *elem = gf.raw(gf._iStorBegin, gf._jCalcEnd - 1);

        for (unsigned int j = 0; j < gf.rows; elem += gf.columns, ++j) {
            right_col[0][j] = *elem;
        }

        check_mpi(
            MPI_Isend(right_col[0], gf.rows, MPI_DOUBLE, gp::right_neighbour, 0, MPI_COMM_WORLD, &send_request[size]),
            "MPI_Isend()::right"
        );

        check_mpi(
            MPI_Irecv(right_col[1], gf.rows, MPI_DOUBLE, gp::right_neighbour, MPI_ANY_TAG, MPI_COMM_WORLD, &recv_req[size++]),
            "MPI_Recv()::right"
        );
    }

    // bottom
    if (gp::cpus_i < gp::cpus_v - 1) {
        check_mpi(
            MPI_Isend(gf.bottomCalc, gf.columns, MPI_DOUBLE, gp::bottom_neighbour, 0, MPI_COMM_WORLD, &send_request[size]),
            "MPI_Isend()::bottom"
        );

        check_mpi(
            MPI_Irecv(gf.bottomStor, gf.columns, MPI_DOUBLE, gp::bottom_neighbour, MPI_ANY_TAG, MPI_COMM_WORLD, &recv_req[size++]),
            "MPI_Recv()::bottom"
        );
    }

    // left
    if (gp::cpus_j > 0) {
        double *elem = gf.raw(gf._iStorBegin, gf._jCalcBegin);

        for (unsigned int j = 0; j < gf.rows; elem += gf.columns, ++j) {
            left_col[0][j] = *elem;
        }

        check_mpi(
            MPI_Isend(left_col[0], gf.rows, MPI_DOUBLE, gp::left_neighbour, 0, MPI_COMM_WORLD, &send_request[size]),
            "MPI_Isend()::left"
        );

        check_mpi(
            MPI_Irecv(left_col[1], gf.rows, MPI_DOUBLE, gp::left_neighbour, MPI_ANY_TAG, MPI_COMM_WORLD, &recv_req[size++]),
            "MPI_Recv()::left"
        );
    }

    // Дожидаемся все ответы
    for (unsigned int i = 0; i < size; ++i) {
        MPI_Status status;

        check_mpi(
            MPI_Wait(&recv_req[i], &status),
            "MPI_Wait()"
        );

        if (status.MPI_SOURCE == gp::right_neighbour) {
            double *elem = gf.raw(gf._iStorBegin, gf._jStorEnd - 1);

            for (unsigned int j = 0; j < gf.rows; elem += gf.columns, ++j) {
                *elem = right_col[1][j];
            }
        }

        if (status.MPI_SOURCE == gp::left_neighbour) {
            double *elem = gf.raw(gf._iStorBegin, gf._jStorBegin);

            for (unsigned int j = 0; j < gf.rows; elem += gf.columns, ++j) {
                *elem = left_col[1][j];
            }
        }
    }

    MPI_Status unused[4];
    MPI_Waitall(size, send_request, unused);
}

int Solver::solve() {
    iteration0();

    do {
        iteration();
        MPI_Allreduce(MPI_IN_PLACE, &diff, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    } while(diff > gp::epsilon);

    return iter_counter;
}

void Solver::dumpSolution() const {
    dump("_phi", _phi);
    dump("_p", _p);
}

void Solver::print(std::ostream &s) const {
    s << 
    "_alpha = " << _alpha << "\n" <<
    "_tao = " << _tao << "\n" <<
    "diff = " << diff << "\n" <<
    "iter_counter = " << iter_counter << std::endl;
}
