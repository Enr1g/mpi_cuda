#!/usr/bin/env python

import os
import sys
from math import exp, log

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from utils import merge_matrices, read_matrix_from_file

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: %s cpus N" % sys.argv[0])

    BASE_DIR = sys.argv[1]
    cpus = int(sys.argv[2])
    N = int(sys.argv[3])

    X = np.arange(-2, 2, 4.0 / (N - 1))[1: N - 1]
    Y = np.arange(-2, 2, 4.0 / (N - 1))[1: N - 1]
    X, Y = np.meshgrid(X, Y)
    Z1, Z2 = merge_matrices(BASE_DIR, cpus, N)

    Z1 = np.asarray(Z1)[::-1]
    Z2 = np.asarray(Z2)[::-1]

    fig = plt.figure(figsize=plt.figaspect(0.5))
    a = fig.add_subplot(121, projection='3d')
    a.plot_surface(X, Y, Z1, cmap=cm.coolwarm)
    a.view_init(30, 135)
        
    a2 = fig.add_subplot(122, projection='3d')
    a2.plot_surface(X, Y, Z2, cmap=cm.coolwarm)
    a2.view_init(30, 135)

    plt.savefig(os.path.join(BASE_DIR, "%s_%s.png" % (cpus, N)))
