#pragma once

#include <iostream>
#include <fstream>
#include <cmath>
#include <mpi.h>

#include "GridFunction.h"
#include "GlobalParameters.h"

// Проверка успешности MPI-вызова
inline void check_mpi(const int return_code, const std::string &error_message) {
    if (return_code != MPI_SUCCESS) {
        throw error_message;
    }
}

template<typename T>
inline void logger(const T &msg, bool endl = true, bool critical=false) {
    // Цветной вывод в терминал
    int m = (gp::mpi_rank / 8) % 2;
    int c = 30 + (gp::mpi_rank + 1) % 8;

    int rank = 0;

    if (critical) {
        while (rank < gp::mpi_size) {
            if (gp::mpi_rank == rank) {
                
                std::cerr << "\033[" << m << ";" << c << "m" << msg << "\033[0m";
                
                if (endl) {
                    std::cerr << std::endl;
                }
            }
            rank++;
            MPI_Barrier(MPI_COMM_WORLD);
        }
    } else {
        std::cerr << "\033[" << m << ";" << c << "m" << msg << "\033[0m";
        
        if (endl) {
            std::cerr << std::endl;
        }
    }
    
}

template<typename T>
inline void dump(const char *filename, const T &item) {
    std::stringstream s;
    s << gp::dump_dir << "/" << filename << "_" << gp::mpi_rank << "_" << gp::mpi_size << "_" << gp::N << ".txt";
    std::fstream f;
    f.open(s.str().c_str(), std::fstream::out);
    f << item;
    f.close();
}

inline void pressAnyKey() {
    if (gp::mpi_rank == 0) {
        getchar();
        MPI_Barrier(MPI_COMM_WORLD);
    } else {
        MPI_Barrier(MPI_COMM_WORLD);
    }
}

template<typename T>
inline void parse(const string &str, T &result) {
    std::istringstream converter(str);
    converter >> result;
}

class DiffLaplasianGetter {
public:
    inline static double get(GridFunction &m, unsigned int i, unsigned int j) {
        // if (!m.boundaryCalcCheck(i, j)) {
        //     GridFunction::makeOOBError("DiffLaplasian::get()", i, j);
        // }

        return m.diffLaplasian(i, j);
    }
};

class WithBoundaryCheckGetter {
public:
    inline static double get(GridFunction &m, unsigned int i, unsigned int j) {
        if (!m.boundaryCalcCheck(i, j)) {
            GridFunction::makeOOBError("WithBoundaryCheckGetter::get()", i, j);
        }

        return m(i, j);
    }
};

class NaiveGetter {
public:
    inline static double get(GridFunction &m, unsigned int i, unsigned int j) {
        return m(i, j);
    }
};

class Phi {
public:
    inline static double fill(unsigned int i, unsigned int j) {
        double x, y;

        x = gp::left + i * gp::h;
        y = gp::top - j * gp::h;
        
        return exp(1 - (x + y) * (x + y));
    }
};

class F {
public:
    inline static double fill(unsigned int i, unsigned int j) {
        double x, y;

        x = gp::left + i * gp::h;
        y = gp::top - j * gp::h;

        return 4 * (1 - 2 * (x + y) * (x + y)) * Phi::fill(i, j);
    }
};

class PhiBorder {
public:
    inline static double fill(unsigned int i, unsigned int j) {
        
        if (i == 0 || i == gp::N - 1 || j == 0 || j == gp::N - 1) {
            return Phi::fill(i, j);
        }

        return 0;
    }
};
