#!/bin/bash

module add slurm
module add impi

set -x

scratch="$HOME/_scratch"

cd $scratch

if [ "$4" ]
then
    sbatch -d after:$4 -n $1 -p test -o "$3/poisson_lm.out" impi ./poisson_lm $2 $3 | cut -d\  -f4
else
    sbatch -n $1 -p test -o "$3/poisson_lm.out" impi ./poisson_lm $2 $3 | cut -d\  -f4
fi

set +x

module rm impi
module rm slurm
