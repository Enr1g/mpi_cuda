#!/bin/bash

set -x

base="lm"
scratch="$HOME/_scratch"

cpus="128 32 16 8"
grids="1000 2000"

for cpu in $cpus
do
    for grid in $grids
    do
        dir="${base}/${cpu}_${grid}"
        mkdir -p "${scratch}/$dir"
        rm -rf ${scratch}/$dir/*
    done
done
