#!/bin/bash

set -x

cpus="512 256 128 1"
grids="1000 2000"

base="omp"

for cpu in $cpus
do
    for grid in $grids
    do
        dir="${base}/${cpu}_${grid}"
        mkdir -p $dir
        pushd $dir
        ../../run_bg_omp.sh $cpu $grid .
        popd
    done
done
