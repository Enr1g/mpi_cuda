#!/bin/sh

set -x

OMPI_CXX=g++-7 mpicxx -Wall -O3 -fopenmp main.cpp Solver.cpp GridFunction.cpp GlobalParameters.cpp -o main.o

