#!/bin/bash

set -x

base=$(dirname $0)
timeout="02:00:00"
[ $1 -eq 512 ] && timeout="00:05:00" 
[ $1 -eq 256 ] && timeout="00:10:00"
[ $1 -eq 128 ] && timeout="00:15:00"

mpisubmit.bg -n $1 -w $timeout -- ${base}/poisson_vanilla $2 $3
