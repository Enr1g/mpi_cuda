#!/bin/sh

set -x

base=$(dirname $0)
cd $base
mpixlcxx_r -O3 -qsmp=omp main.cpp GlobalParameters.cpp GridFunction.cpp Solver.cpp -o poisson_omp
