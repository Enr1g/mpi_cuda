#include <iostream>
#include <mpi.h>

#include "utils.h"
#include "Solver.h"

int main(int argc, char **argv) {
    try {
        check_mpi(MPI_Init(&argc, &argv), "MPI_Init");

        if (argc < 3) {
            std::cout << "Usage: " << argv[0] << "N result_dir" << std::endl;
            throw "Not enough arguments";
        }

        check_mpi(MPI_Comm_rank(MPI_COMM_WORLD, &gp::mpi_rank), "Failed to get rank");
        check_mpi(MPI_Comm_size(MPI_COMM_WORLD, &gp::mpi_size), "Failed to get size");

        parse(argv[1], gp::N);
        gp::top = 2.0;
        gp::bottom = -2.0;
        gp::left = -2.0;
        gp::right = 2.0;
        gp::epsilon = 1e-4;
        gp::dump_dir = argv[2];
        
        gp::Init();

        Solver solver;

        double start;

        if (gp::mpi_rank == 0) {
            start = MPI_Wtime();
        }

        unsigned int iter_counter = solver.solve();
        MPI_Barrier(MPI_COMM_WORLD);

        if (gp::mpi_rank == 0) {
            std::cout << MPI_Wtime() - start << std::endl;
            std::cout << iter_counter << std::endl;
        }

        solver.dumpSolution();

    } catch(string error_message) {
        logger(error_message);
    }

    MPI_Finalize();

    return 0;
}
