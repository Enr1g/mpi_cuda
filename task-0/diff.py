#!/usr/bin/env python
import sys
from utils import mass_diff

if __name__ == "__main__":
    BASE_DIR = sys.argv[1]
    cpus = int(sys.argv[2])
    N = int(sys.argv[3])
    h = 4.0 / N

    m = mass_diff(BASE_DIR, cpus, N)
    
    print(m)
