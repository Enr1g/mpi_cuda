#include "GlobalParameters.h"

// Объявление статических членов класса
unsigned int gp::N;
double gp::h;
double gp::rh;

int gp::mpi_size;
int gp::mpi_rank;

unsigned int gp::cpus_h;
unsigned int gp::cpus_v;
unsigned int gp::cpus_i;
unsigned int gp::cpus_j;
unsigned int gp::top_neighbour;
unsigned int gp::right_neighbour;
unsigned int gp::bottom_neighbour;
unsigned int gp::left_neighbour;

double gp::top;
double gp::right;
double gp::bottom;
double gp::left;

double gp::epsilon;

char *gp::dump_dir;


void GlobalParameters::Init() {
    /*
     * cpus_h = 2^h процессоров по горизонтали
     * cpus_v = 2^v процессоров по вертикали
     * 0.5 <= cpus_h / cpus_v <= 2
     */
    unsigned int n, hor, ver;
    n = round(log2(mpi_size));
    hor = n >> 1;
    ver = n - hor;

    cpus_h = 1 << hor;
    cpus_v = 1 << ver;

    /*
     * Получение индексов cpus_i, cpus_j текущего
     * вычислителя по его рангу.
     */
    cpus_i = mpi_rank / cpus_h;
    cpus_j = mpi_rank % cpus_h;

    top_neighbour = cpus_i != 0 ? (cpus_i - 1) * cpus_h + cpus_j : -1;
    bottom_neighbour = cpus_i != cpus_v - 1 ? (cpus_i + 1) * cpus_h + cpus_j : -1;
    left_neighbour = cpus_j != 0 ? cpus_i * cpus_h + cpus_j - 1 : -1;
    right_neighbour = cpus_j != cpus_h - 1 ? cpus_i * cpus_h + cpus_j + 1 : -1;

    /*
     * У нас по условию квадрат, так что шаг равномерной
     * сетки совпадает по горизонтали и вертикали
     */
    h = (gp::top - gp::bottom) / (gp::N - 1);

    rh = 1 / (h * h);
}

void GlobalParameters::print(std::ostream &s) {
    s << 
    "gp::N = " << gp::N << "\n" <<
    "gp::h = " << gp::h << "\n" <<

    "gp::mpi_size = " << gp::mpi_size << "\n" <<
    "gp::mpi_rank = " << gp::mpi_rank << "\n" <<

    "gp::cpus_h = " << gp::cpus_h << "\n" <<
    "gp::cpus_v = " << gp::cpus_v << "\n" <<
    "gp::cpus_i = " << gp::cpus_i << "\n" <<
    "gp::cpus_j = " << gp::cpus_j << "\n" <<
    "gp::top_neighbour = " << gp::top_neighbour << "\n" <<
    "gp::right_neighbour = " << gp::right_neighbour << "\n" <<
    "gp::bottom_neighbour = " << gp::bottom_neighbour << "\n" <<
    "gp::left_neighbour = " << gp::left_neighbour << "\n" <<

    "gp::top = " << gp::top << "\n" <<
    "gp::right = " << gp::right << "\n" <<
    "gp::bottom = " << gp::bottom << "\n" <<
    "gp::left = " << gp::left << "\n" <<

    "gp::epsilon = " << gp::epsilon << "\n" <<

    "gp::dump_dir = " << gp::dump_dir << "\n";
}
