#!/bin/bash

set -x

mpirun -n $1 -- main.o $2 $3 2> >(c++filt -n)

