from math import log
from os import path

P_MASK = '_p_%s_%s_%s.txt'
PHI_MASK = '_phi_%s_%s_%s.txt'


def read_matrix(s):
    m = [[float(j) for j in i.strip().split(' ')] for i in s.strip().split('\n')]
    return m


def mzip(m1, m2):
    for i1, i2 in zip(m1, m2):
        for j1, j2 in zip(i1, i2):
            yield (j1, j2)


def dot(h, m1, m2):
    s = 0

    for j1, j2 in mzip(m1, m2):
        s += j1 * j2 * h**2
    
    return s


def read_matrix_from_file(filename):
    with open(filename, 'r') as f:
        m = read_matrix(f.read())
    
    return m


def diff(m1, m2):
    return max((j1 - j2) for j1, j2 in mzip(m1, m2))


def mass_diff(BASE_DIR, cpus, N):
    res = 0

    for cpu in range(cpus):
        m1 = read_matrix_from_file(path.join(BASE_DIR, P_MASK % (cpu, cpus, N)))
        m2 = read_matrix_from_file(path.join(BASE_DIR, PHI_MASK % (cpu, cpus, N)))
        res = max(diff(m1, m2), res)
    
    return res


def merge_matrices(BASE_DIR, cpus, N):
    hor, ver = grid(cpus)
    m1, m2 = [], []

    for i in range(ver):
        rows = len(open(path.join(BASE_DIR, PHI_MASK % (i * hor, cpus, N))).readlines())
        
        m1row = [[] for _ in range(rows)]
        m2row = [[] for _ in range(rows)]

        for cpu in range(i * hor, (i + 1) * hor):
            # print(i, cpu)

            list(map(list.extend, m1row, read_matrix_from_file(path.join(BASE_DIR, PHI_MASK % (cpu, cpus, N)))))
            list(map(list.extend, m2row, read_matrix_from_file(path.join(BASE_DIR, P_MASK % (cpu, cpus, N)))))

        m1.extend(m1row)
        m2.extend(m2row)

    return m1, m2


def grid(cpus):
    n = int(log(cpus, 2))
    hor = n >> 1
    ver = n - hor

    return (1 << hor, 1 << ver)
