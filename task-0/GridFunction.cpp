#include "GridFunction.h"
#include "utils.h"

// Объявление статических членов класса
unsigned int GridFunction::_iCalcBegin;
unsigned int GridFunction::_iCalcEnd;
unsigned int GridFunction::_jCalcBegin;
unsigned int GridFunction::_jCalcEnd;

unsigned int GridFunction::_iStorBegin;
unsigned int GridFunction::_iStorEnd;
unsigned int GridFunction::_jStorBegin;
unsigned int GridFunction::_jStorEnd;

unsigned int GridFunction::rows;
unsigned int GridFunction::columns;

bool GridFunction::_inited = false;

void GridFunction::Init() {
    /*
     * Вычисление границ сетки, за которые
     * отвечает текущий вычислитель, и
     * граничных точек, необходимые для
     * вычисления разностного оператора
     * Лапласа
     */

    unsigned int avg_per_cpu_v, avg_per_cpu_h;

    avg_per_cpu_v = gp::N / gp::cpus_v;
    avg_per_cpu_h = gp::N / gp::cpus_h;

    _iCalcBegin = std::max(1u, gp::cpus_i * avg_per_cpu_v);
    _iCalcEnd = (gp::cpus_i + 1) * avg_per_cpu_v;

    _jCalcBegin = std::max(1u, gp::cpus_j * avg_per_cpu_h);
    _jCalcEnd = (gp::cpus_j + 1) * avg_per_cpu_h;

    // "Правые" и "нижние" процессоры обрабатывают оставшиеся части
    if (gp::cpus_i == gp::cpus_v - 1) {
        _iCalcEnd = gp::N - 1;
    }

    if (gp::cpus_j == gp::cpus_h - 1) {
        _jCalcEnd = gp::N - 1;
    }

    _iStorBegin = _iCalcBegin - 1;
    _iStorEnd = _iCalcEnd + 1;
    _jStorBegin = _jCalcBegin - 1;
    _jStorEnd = _jCalcEnd + 1;

    rows = _iStorEnd - _iStorBegin;
    columns = _jStorEnd - _jStorBegin;

    _inited = true;
}

GridFunction::GridFunction() {
    if (!_inited) {
        Init();
    }

    _GridFunction = new double[rows * columns];
    topStor = this->raw(_iStorBegin, _jStorBegin);
    bottomStor = this->raw(_iStorEnd - 1, _jStorBegin);
    topCalc = this->raw(_iCalcBegin, _jStorBegin);
    bottomCalc = this->raw(_iCalcEnd - 1, _jStorBegin);
}

GridFunction::~GridFunction() {
    delete[] _GridFunction;
}

double &GridFunction::operator()(unsigned int i, unsigned int j) {
    return _GridFunction[(i - _iStorBegin) * columns + (j - _jStorBegin)];
}

const double &GridFunction::operator()(unsigned int i, unsigned int j) const {
    return _GridFunction[(i - _iStorBegin) * columns + (j - _jStorBegin)];
}

const double *GridFunction::raw(unsigned int i, unsigned int j) const {
    return &_GridFunction[(i - _iStorBegin) * columns + (j - _jStorBegin)];
}

double *GridFunction::raw(unsigned int i, unsigned int j) {
    return &_GridFunction[(i - _iStorBegin) * columns + (j - _jStorBegin)];
}

double GridFunction::diffLaplasian(unsigned int i, unsigned int j) const {
    const double *mij = this->raw(i, j);

    // if (!boundaryCalcCheck(i, j)) {
    //     makeOOBError("diffLaplasian()", i, j);
    // }

    double result = 4 * *mij - *(mij - 1) - *(mij + 1) - *(mij - columns) - *(mij + columns);
    result *= gp::rh;

    return result;
}

bool GridFunction::boundaryStorCheck(unsigned int i, unsigned int j) const {
    return i >= _iStorBegin || i < _iStorEnd || j >= _jStorBegin || j < _jStorEnd;
}

bool GridFunction::boundaryCalcCheck(unsigned int i, unsigned int j) const {
    return i >= _iCalcBegin || i < _iCalcEnd || j >= _jCalcBegin || j < _jCalcEnd;
}

double GridFunction::dot(const GridFunction &other) const {
    double result = 0;

    #pragma omp parallel for reduction(+:result)
    for (int i = _iCalcBegin; i < _iCalcEnd; ++i) {
        for (int j = _jCalcBegin; j < _jCalcEnd; ++j) {
            // if (!m.boundaryCalcCheck(i, j)) {
            //     GridFunction::makeOOBError("dot()::m", i, j);
            // }

            // if (!other.boundaryCalcCheck(i, j)) {
            //     GridFunction::makeOOBError("dot()::other", i, j);
            // }

            result += (*this)(i, j) * other(i, j);
        }
    }

    return result * (gp::h * gp::h);
}

double GridFunction::dot_dlp(const GridFunction &other) const {
    double result = 0;

    #pragma omp parallel for reduction(+:result)
    for (int i = _iCalcBegin; i < _iCalcEnd; ++i) {
        for (int j = _jCalcBegin; j < _jCalcEnd; ++j) {
            // if (!m.boundaryCalcCheck(i, j)) {
            //     GridFunction::makeOOBError("dot()::m", i, j);
            // }

            // if (!other.boundaryCalcCheck(i, j)) {
            //     GridFunction::makeOOBError("dot()::other", i, j);
            // }

            result += this->diffLaplasian(i, j) * other(i, j);
        }
    }

    return result * (gp::h * gp::h);
}

void GridFunction::makeOOBError(const string &location, unsigned int i, unsigned int j) {
    std::stringstream err;
    err << "Out of bound error in " << location << ": (" << i << ", " << j << ")" << std::endl;
    throw err.str();
}

void GridFunction::print(std::ostream &s) {
    s << 
    "GridFunction::_iCalcBegin = " << GridFunction::_iCalcBegin << "\n" <<
    "GridFunction::_iCalcEnd = " << GridFunction::_iCalcEnd << "\n" <<
    "GridFunction::_jCalcBegin = " << GridFunction::_jCalcBegin << "\n" <<
    "GridFunction::_jCalcEnd = " << GridFunction::_jCalcEnd << "\n" <<
    "GridFunction::_iStorBegin = " << GridFunction::_iStorBegin << "\n" <<
    "GridFunction::_iStorEnd = " << GridFunction::_iStorEnd << "\n" <<
    "GridFunction::_jStorBegin = " << GridFunction::_jStorBegin << "\n" <<
    "GridFunction::_jStorEnd = " << GridFunction::_jStorEnd << "\n" <<
    "GridFunction::rows = " << GridFunction::rows << "\n" <<
    "GridFunction::columns = " << GridFunction::columns << "\n" <<
    "GridFunction::_inited = " << GridFunction::_inited << "\n";
}

std::ostream &operator<<(std::ostream &s, const GridFunction &gf) {
    for (unsigned int i = gf._iCalcBegin; i < gf._iCalcEnd; ++i) {
        for (unsigned int j = gf._jCalcBegin; j < gf._jCalcEnd; ++j) {
            s << gf(i, j) << " ";
        }

        s << std::endl;
    }

    return s;
}
