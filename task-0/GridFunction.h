#pragma once

#include <sstream>
#include <string>
#include <cmath>

#include "GlobalParameters.h"

using std::string;

class GridFunction {
    double *_GridFunction;
public:
    // Границы вычисления оператора Лапласа
    static unsigned int _iCalcBegin;
    static unsigned int _iCalcEnd;
    static unsigned int _jCalcBegin;
    static unsigned int _jCalcEnd;

    // Границы хранения значений сетки
    static unsigned int _iStorBegin;
    static unsigned int _iStorEnd;
    static unsigned int _jStorBegin;
    static unsigned int _jStorEnd;

    static unsigned int rows;
    static unsigned int columns;

    static bool _inited;

    GridFunction();

    ~GridFunction();

    /*
     * Инициализация глобальных параметров
     * сеточных функций
     */
    static void Init();

    /*
     * Функции двойного индексирования.
     * Не проверяют выход за границу
     */
    double &operator()(unsigned int i, unsigned int j);
    const double &operator()(unsigned int i, unsigned int j) const;

    /*
     * Получить указатель на внутреннюю часть массива
     */
    double *raw(unsigned int i, unsigned int j);
    const double *raw(unsigned int i, unsigned int j) const;

    // Разностный оператор Лапласа
    double diffLaplasian(unsigned int i, unsigned int j) const;

    /*
     * Функции проверок выхода за границу
     * для границ хранения и вычисления
     */
    bool boundaryStorCheck(unsigned int i, unsigned int j) const;
    bool boundaryCalcCheck(unsigned int i, unsigned int j) const;

    /*
     * Скалярное произведение
     */
    double dot(const GridFunction &other) const;

    /*
     * Скалярное произведение с разностным
     * оператором Лапласа
     */
    double dot_dlp(const GridFunction &other) const;

    /*
     * Функция, заполняющая сетку при помощи заданной функции
     */
    template<typename Filler>
    void genFill() {
        for (unsigned int i = _iStorBegin; i < _iStorEnd; ++i) {
            for (unsigned int j = _jStorBegin; j < _jStorEnd; ++j) {
                (*this)(i, j) = Filler::fill(i, j);
            }
        }
    }

    /*
     * Дебаговая функция для вывода Out-of-Bound ошибок
     */
    static void makeOOBError(const string &location, unsigned int i, unsigned int j);

    /*
     * Возвращает ссылку на первую строку матрицы,
     * предназначенную для хранения
     */
    double *topStor;

    /*
     * Возвращает ссылку на последнюю строку матрицы,
     * предназначенную для хранения
     */
    double *bottomStor;

    /*
     * Возвращает ссылку на первую строку матрицы,
     * предназначенную для вычисления
     */
    double *topCalc;

    /*
     * Возвращает ссылку на последнюю строку матрицы,
     * предназначенную для вычисления
     */
    double *bottomCalc;

    /*
     * Распечатать параметры матрицы
     */
    static void print(std::ostream &s);

    /*
     * Вывод сеточной функции на поток
     */
    friend std::ostream &operator<<(std::ostream &s, const GridFunction &gf);
};
