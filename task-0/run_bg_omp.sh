#!/bin/bash

set -x

base=$(dirname $0)
timeout="02:00:00"
[ $1 -eq 512 ] && timeout="00:05:00" 
[ $1 -eq 256 ] && timeout="00:10:00"
[ $1 -eq 128 ] && timeout="00:15:00"

mpisubmit.bg -n $1 -e "OMP_NUM_THREADS=4" -w $timeout -- ${base}/poisson_omp $2 $3
